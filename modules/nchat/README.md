# nchat

#### Table of Contents

1. [Overview](#overview)
2. [Module Description - What the module does and why it is useful](#module-description)
3. [Setup - The basics of getting started with nchat](#setup)
    * [What nchat affects](#what-nchat-affects)
    * [Beginning with nchat](#beginning-with-nchat)
4. [Usage - Configuration options and additional functionality](#usage)
5. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)

## Overview
This is a toy module that encapsulates the installation and acitivation an "irc" clone based on `nmap-nac` and a `systemd` service file.

## Module Description
The module is comprised of a single class (within init.pp) and a file (the service file: `nchat.service`)

## Setup

### What nchat affects

* installs `nchat.service` to `/etc/systemd/system/`
* installs the `nmap-ncat` package as a dependency
* activates the `nchat` service for the `multi-user.target`

### Beginning with nchat
Simply include the nchat class within a node. It takes no parameters.

## Usage
See above

## Reference

This module uses the following resource types:

* file
* package
* service

## Limitations

This module only works on CentOS 7.


