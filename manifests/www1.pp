node 'www1.vpc1.nasp' {
	include httpd

	file { '/tmp/node_name':
		content => $::hostname,
	}
}
