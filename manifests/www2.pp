node 'www2.vpc1.nasp' {
	include httpd
	include nchat

	file { '/tmp/node_name':
		content => $::hostname,
	}
}
